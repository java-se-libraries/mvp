package geek.bobby.java8.MVP;


import org.jetbrains.annotations.NotNull;

/**
	An abstract view which can return a root view. Generic type T can be any class, mostly a graphical window
*/
public interface IView<T>
{
	/**
	*	@return The root view of this abstract view
	*/
	@NotNull
	T getRootView();
}
