package geek.bobby.java8.MVP;

import org.jetbrains.annotations.NotNull;

/**
	An abstract presenter accepts a view, and handles its parnet's destruction
*/
public interface IPresenter<T extends IView>
{
	/**
	*	Sets a view to be used by this presenter
	*	@param view a view extending abstact IView
	*/
	void setView(@NotNull T view);

	/**
	*	Meta-controllers like activities and fragments will call this method on their destruction.
	*	Any clearing up can be done here
	*/
	void onDestroy();
}
