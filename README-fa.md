# کتابخانه‌ٔ MVP
این کتابخانه مجموعه ای از واسطهای جاوا است که برای تعریف و پیاده سازی معماری Model-View-Presenter در نرم افزارهای جاوا و به ویژه اندروید به کار می روند. هم اکنون دو واسط تعریف شده اند:

* واسط `IView` که پنجرهٔ ریشهٔ خودش را اعلان می‌کند. این پنجرهٔ ریشه می تواند یک پنجرهٔ گرافیکی واقعی باشد.
* واسط `Ipresenter` که یک view دریافت می‌کند و درصورت پایان عمر شی والد خود می تواند کارهایی را انجام دهد.

## مراجع

[android MVP for beginners](https://android.jlelse.eu/android-mvp-for-beginners-25889c500443)

[model-view-presenter android guidelines](https://medium.com/@cervonefrancesco/model-view-presenter-android-guidelines-94970b430ddf)

[Architectural Guidelines to follow for MVP pattern in Android](https://android.jlelse.eu/architectural-guidelines-to-follow-for-mvp-pattern-in-android-2374848a0157)

[Voggella's tutorial](https://www.vogella.com/tutorials/AndroidArchitecture/article.html)

[Martin Fowler's article about GUI Architectures](https://www.martinfowler.com/eaaDev/uiArchs.html)

[Interactive application architecture](http://aspiringcraftsman.com/2007/08/25/interactive-application-architecture/)
