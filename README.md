# MVP library
This is a set of base java interfaces for defining Model-View-Presenter architectures in java and specially android applications. Here we define these two interacers:

* `Iview` An abstracted view which can report its root view. The root view can be a real graphical view.
* `IPresenter` A presenter which takes a view and also can take actions if its parent gets destroyed.

[نسخهٔ فارسی](https://framagit.org/java-se-libraries/mvp/blob/master/README-fa.md)

## References

[android MVP for beginners](https://android.jlelse.eu/android-mvp-for-beginners-25889c500443)

[model-view-presenter android guidelines](https://medium.com/@cervonefrancesco/model-view-presenter-android-guidelines-94970b430ddf)

[Architectural Guidelines to follow for MVP pattern in Android](https://android.jlelse.eu/architectural-guidelines-to-follow-for-mvp-pattern-in-android-2374848a0157)

[Voggella's tutorial](https://www.vogella.com/tutorials/AndroidArchitecture/article.html)

[Martin Fowler's article about GUI Architectures](https://www.martinfowler.com/eaaDev/uiArchs.html)

[Interactive application architecture](http://aspiringcraftsman.com/2007/08/25/interactive-application-architecture/)


